Categories:Time,Internet
License:GPL-3.0
Web Site:
Source Code:https://github.com/hypeapps/episodie
Issue Tracker:https://github.com/hypeapps/episodie/issues

Auto Name:Episodie
Summary:Discover and track TV show time
Description:
Episodie is a TV show time tracker app with unusual design. Get to know how much
time you spent watching tv shows. Track easily overall progress of your favorite
shows and discover new ones. Stay up to date with new premieres thanks to
notifications.
.

Repo Type:git
Repo:https://github.com/hypeapps/episodie.git

Build:1.0.1,2
    commit=1.0.1
    subdir=application
    patch=repositories.patch
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
